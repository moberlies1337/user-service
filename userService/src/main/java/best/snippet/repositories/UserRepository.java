package best.snippet.repositories;

import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import best.snippet.models.User;

public interface UserRepository extends MongoRepository<User, ObjectId> {

	public List<User> findByFirstNameAndLastName(String firstName, String lastName);
	public User findByEmail(String email);
}
