package best.snippet.services;

import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import best.snippet.models.User;
import best.snippet.repositories.UserRepository;

@Component
public class UserService {

	@Autowired
	private UserRepository repository;

	public User create(User s) {
		return repository.save(s);
	}

	public List<User> findAll() {
		return repository.findAll();
	}

	public User findById(ObjectId id) {
		return repository.findById(id).orElse(null);
	}
	
	public User findByEmail(String email) {
		return repository.findByEmail(email);
	}

	public User update(User s) {
		return repository.save(s);
	}

	public void delete(ObjectId id) {
		repository.deleteById(id);
	}

	public void deleteAll() {
		repository.deleteAll();
	}
}
