package best.snippet.controllers;

import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import best.snippet.models.User;
import best.snippet.services.UserService;

@CrossOrigin("*")
@RestController
@RequestMapping("/users")
public class UserController {
	@Autowired
	private UserService service;

	@PostMapping
	public User create(@RequestBody User s) {
		return service.create(s);
	}
	
	@GetMapping
	public List<User> findAll() {
		return service.findAll();
	}

	@GetMapping("/{id}")
	public User find(@PathVariable("id") ObjectId id) {
		return service.findById(id);
	}
	
	@GetMapping("/email/{email}")
	public User findByEmail(@PathVariable("email") String email) {
		return service.findByEmail(email);
	}
	
	@PatchMapping
	public User update(@RequestBody User s) {
		return service.update(s);
	}
	
	@DeleteMapping("/{id}")
	public void delete(@PathVariable("id") ObjectId id) {
		service.delete(id);
	}
}
